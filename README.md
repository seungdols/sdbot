///This AI is simple and make rule-based/////

///My Strategy is useful Projectile and AIR_F(Foot) Skills

//AI get high score in AI class of Univ. so I submit  FTG



//Environment : JDK8 , Eclipse 
//FTG library download : http://www.ice.ci.ritsumei.ac.jp/~ftgaic/index.htm

- Introduction

AI Name : SDBOT

Character : ZEN

Advisor : Kyung-Joong Kim

Developer Name: Seung-Ho Choi

Affiliation
Dept. of Computer Engineering, Sejong Univ.

- AI’s Outline

1) To reduce  distance with opponent – use skills
but those are a priority among the skills.
According to the skill in using the determined distance value and pseudo-random value.

Primary skill : STAND_D_DF_FA , STAND_D_DF_FB , STAND_D_DF_FA

2) Move
if  x point of my character large than x point of opponent character
→ inputKey.L
else
→ inputKey.R  

3) attack

primary skills : STAND_D_DF_FA , STAND_D_DF_FB , STAND_D_DF_FA
sub-skills : AIR_B, AIR_UB

- Strategic

AI is a rules-based strategy is adopted by a lot of use projectile and jump skill.

There are advantages to having the use of high projectile that score points.

Get the energy, just use the projectile and to avoid the projectiles well but it is still unfinished.


This strategy could be the No. 1 in the competition of AI class in university.

- Point


This Code is simple so powerfull.


Distance base-action is so good-reward in this AI game.


The strategy of a number of experiments were able to get the highest score.


- Weakness


This SDBOT isn’t good AI because of rule-base. 

Seaching method or Machine-Learning  not apply.

SDBOT have weak in specific skils.(Catching, Same jump skills, rapidly frame skills) 


Thanks.

Contact Mail : choiseungho0822@gmail.com