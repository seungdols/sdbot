import enumerate.Action;
import gameInterface.AIInterface;

import java.util.Random;

import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import structs.MotionData;

import commandcenter.CommandCenter;
/*
 * AI name is SDBOT.
 * This destination of code is more getting high-score.
 * This code is rule-based AI and access not data file, don't store file.
 * designed by Seungho-choi (Computer Engineering Dep. Sejong Univ. South Korea)
 * contact : choiseungho0822@gmail.com
 */
public class SDBOT implements AIInterface {
	/*
	 * variable
	 */
	GameData gd;
	Key inputKey;
	boolean playerNumber;
	FrameData frameData;
	CommandCenter command;
	int distance = 0;

	String myAction = null;
	Action enemyAct;
	Action myAct;
	int attack_type = 0;
	CharacterData my_AI;
	CharacterData other;

	Random rnd;
	Random disRandom;
	int disRan;
	MotionData oppMotion;
	MotionData myMotion;
	
	//variable init
	public synchronized int initialize(GameData gameData, boolean playerNumber) {
		
		gd = gameData;
		this.playerNumber = playerNumber;
		this.inputKey = new Key();
		command = new CommandCenter();
		frameData = new FrameData();
		rnd = new Random();
		disRandom = new Random();
		oppMotion = new MotionData();
		myMotion = new MotionData();
		return 0;
		 
	}
	/*
	 * setting framedata in parameter Framedata 
	 * @see gameInterface.AIInterface#getInformation(structs.FrameData)
	 */
	public synchronized void getInformation(FrameData frameData) {
		this.frameData = frameData;

		command.setFrameData(this.frameData, playerNumber);

	}

	public synchronized void processing() {

		if (!frameData.getEmptyFlag() && frameData.getRemainingTime() > 0) {

			//get character data
			other = command.getEnemyCharacter();
			my_AI = command.getMyCharacter();

			int posMY = my_AI.getX();
			int posxOther = other.getX();

			// get Distance
			distance = command.getDistanceX();
			
			enemyAct = other.getAction();
			myAct = my_AI.getAction();
			
			if (playerNumber){
				oppMotion = gd.getPlayerTwoMotion().elementAt(
						enemyAct.ordinal());
				myMotion = gd.getPlayerOneMotion().elementAt(myAct.ordinal());
			}
			else
			{
				oppMotion = gd.getPlayerOneMotion().elementAt(
						enemyAct.ordinal());
				myMotion = gd.getPlayerTwoMotion().elementAt(myAct.ordinal());
			}
			attack_type = oppMotion.getAttackType();
			//create random number 
			disRan = disRandom.nextInt(7)+1;

			if (command.getskillFlag()) {

				inputKey = command.getSkillKey();

			} else {

				inputKey.empty();
				command.skillCancel();

				if (distance >= 300 && my_AI.getEnergy() < 30) {
					command.commandCall("9");
					command.commandCall("STAND_D_DF_FA");
				}

				if (distance < 300 + disRan) {
					command.commandCall("AIR_UB");

					//
				} else if (distance < 150 + disRan) {
					command.commandCall("AIR_B");
				}
				else if (distance < 150 + disRan){
					command.commandCall("AIR_FB");
				}
				if (distance > 150 + disRan && distance < 535
						&& my_AI.getEnergy() < 30) {
					command.commandCall("STAND_D_DF_FA");

				}
				if (command.getMyEnergy() >= 300) {

					command.commandCall("STAND_D_DF_FC");
				}

				if (distance >= 150 && distance < 535
						&& my_AI.getEnergy() >= 30) {
					command.commandCall("STAND_D_DF_FB");

				}

				if (distance >= 300 + disRan
						&& oppMotion.equals("STAND")) {
					command.commandCall("STAND_D_DF_FA");
				}
				
				if((posMY + posMY) < 90 )
				{
					command.commandCall("CROUCH_B");
				}
				/*
				 * get energy eq 30,  just using skill
				 */
				if (my_AI.getEnergy() >= 30) {
					command.commandCall("STAND_D_DF_FB");

				} else if (distance > 170 + disRan) {
					if (oppMotion.equals("AIR"))
						command.commandCall("8");
					command.commandCall("STAND_D_DF_FA");
				}
				/*
				 * avoid skill
				 */
				if (enemyAct.equals("STAND_D_DF_FB")
						|| enemyAct.equals("STAND_D_DF_FC")
						|| enemyAct.equals("STAND_D_DF_FA")) {
					command.commandCall("9");
				}
				/*
				 * Air projectile
				 */
				if(distance >= 150 && oppMotion.equals("STAND")){
					command.commandCall("AIR_D_DF_FA");
				}
				if (my_AI.getEnergy() >= 40) {
					command.commandCall("AIR_F_D_DFB");
				}
				
				/*
				 * move event
				 */
				if (posMY > posxOther) {
					inputKey.L = true;
				} else {
					inputKey.R = true;
				}
			}
		}
	}

	public synchronized Key input() {
		// TODO Auto-generated method stub
		return inputKey;

	}

	public synchronized void close() {
		// TODO Auto-generated method stub

	}

	public String getCharacter() {
		// TODO Auto-generated method stub
		return CHARACTER_ZEN;
	}

}
